package com.Cortinas.Cortinas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CortinasApplication {

	public static void main(String[] args) {
		SpringApplication.run(CortinasApplication.class, args);
	}

}
